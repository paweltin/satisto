$.fn.extend({
	message: function () {
		var $nextEl = this.next();
		this.messageHide = function($el) {
			$el = $el || this.find('.message');

			var height = 0;
			$el.each(function() {
				height += $(this).outerHeight();
			});

			$nextEl.css('padding-top', parseInt($nextEl.css('padding-top')) - height);
			$el.animate({height: 0, padding: 0}, 100, function() {
				this.remove();
			});
		};

		this.find('.message-close').on('click', (function(e) {
			this.messageHide($(e.currentTarget).parents('.message'));
		}).bind(this));

		setTimeout((function() {
			this.addClass('show');
			$nextEl.css({
				'padding-top': parseInt($nextEl.css('padding-top')) + this.outerHeight(),
				'transition': 'padding .2s ease'
			});
		}).bind(this), 500);

		setTimeout((function() {
			this.messageHide();
		}).bind(this), 10000);
	}
});
