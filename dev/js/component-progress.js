$.fn.extend({
	progress: function () {
		this.$progressBar = this.find('[data-width]');

		if (this.$progressBar.length === 0) {
			return false;
		}

		this.$progressBar.width(0);

		var width = this.$progressBar.data('width');
		this.$progressBar.width(width);

		var widthParsed = parseInt(width);

		if (widthParsed <= 25) {
			this.addClass('progress--danger');
		} else if (widthParsed <= 50) {
			this.addClass('progress--warn');
		}
	}
});
