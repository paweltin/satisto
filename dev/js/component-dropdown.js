$.fn.extend({
	dropdown: function() {
		$('.dropdown-item').on('click', function(e) {
			var $target = $(e.currentTarget);
			var $dropdown = $target.parents('.dropdown');
			var $dropdownToggle = $dropdown.find('.dropdown-toggle');

			$dropdown.find('.dropdown-item').removeClass('active');
			$target.addClass('active');
			$dropdownToggle.html($target.html());
		});
	},
	customToggle: function() {
		$('[data-custom-toggled-group]').hide();

		function checkGroup($el) {
			$('[data-custom-toggled-group=' + $el.data('customToggleGroup') + ']').fadeOut(200, function () {
				$(this).find('[data-custom-toggled]').hide();
				$('[data-custom-toggled=' + $el.data('customToggle') + '-toggle]').show();
				$(this).fadeIn(200);
			});
		}

		$('.dropdown-toggle').each(function (i, el) {
			checkGroup($(el));
		});

		this.on('click', function(e) {
			checkGroup($(e.currentTarget));
		});
	}
});
