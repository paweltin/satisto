require('bootstrap');
global.$ = require('jquery');
var Chart = require('chart.js');
require('slick-carousel');

require('./component-custom-carousel');
require('./component-chart');
require('./component-dropdown');
require('./component-form');
require('./component-message');
require('./component-progress');

$('.chart-donut').each(function(i, el) {
	$(el).chartDonut();
});
$('.chart-donut-2').each(function(i, el) {
	$(el).chartDonut2();
});
$('.chart-line').each(function(i, el) {
	$(el).chartLine();
});
$('.dropdown').dropdown();
$('.dropdown-item').customToggle();
$('.form-group--float-label, .input-group--float-label').floatLabel();
$('.form-buy').formCheckButton();
$('.input-remind').formCheckRadio();
$('.message-container').message();
$('.progress').progress();

hljs.registerLanguage('javascript', require('highlight.js/lib/languages/javascript'));
hljs.initHighlightingOnLoad();
hljs.initLineNumbersOnLoad();
