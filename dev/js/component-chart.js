$.fn.extend({
	windowWidth: $(window).width(),
	threshold: 768,
	isMobile: function() {
		return this.windowWidth <= this.threshold
	},
	initializeButtons: function(chart) {
		var $legendItems = $(this).siblings('.chart-legend').find('.chart-legend-item');

		$legendItems.each(function(i, el) {
			$(el).on('click', function(e) {
				var index = $(e.currentTarget).index();
				var meta = chart.getDatasetMeta(index);

				meta.hidden = meta.hidden === null ? !chart.data.datasets[index].hidden : null;
				chart.update();
			})
		});
	},
	initializeButtonsDonut: function(chart) {
		var $legendItems = $(this).siblings('.chart-legend').find('.chart-legend-item');

		$legendItems.each(function(i, el) {
			$(el).on('click', function(e) {
				var index = $(e.currentTarget).index();
				var meta = chart.getDatasetMeta(0);

				meta.data[index].hidden = !meta.data[index].hidden;
				chart.update();
			})
		});
	},
	initializeButtonsDonut2: function(chart) {
		var $legendItems = $(this).parents('.box').find('.report-item');

		$legendItems.each(function(i, el) {
			$(el).on('click', function(e) {
				var index = $(e.currentTarget).index('.report-item');
				var meta = chart.getDatasetMeta(0);

				meta.data[index].hidden = !meta.data[index].hidden;
				chart.update();
			})
		});
	},
	chartDonut: function() {
		if (this.length === 0) {
			return false
		}

		if (this.isMobile()) {
			this.attr('height', parseInt($(this).attr('width')) * .75);
		}

		var c = new Chart(this, {
			type: 'doughnut',
			data: {
				datasets: [{
					data: $(this).data().dataset,
					backgroundColor: ['#29CC97', '#FFCB68', '#FD668A'],
					borderWidth: 0
				}],
				labels: ['Dobrze', 'Średnio', 'Słabo']
			},
			options: {
				animation: {
					duration: 200
				},
				legend: false,
				cutoutPercentage: 75,
				scales: {
					xAxes: [{
						gridLines: {
							display: false,
							drawBorder: false
						},
						ticks: {
							padding: 12,
							fontSize: 0
						}
					}],
					yAxes: [{
						display: false
					}]
				},
				tooltips: {
					enabled: false,
					custom: function(tooltip) {
						// Tooltip Element
						var $tooltipEl = $('.chart-tooltip');

						if (!$tooltipEl.length) {
							$tooltipEl = $('<div>');
							$tooltipEl.addClass('chart-tooltip');
						} else {
							$tooltipEl.empty();
						}
						this._chart.canvas.parentNode.appendChild($tooltipEl[0]);

						// Hide if no tooltip
						if (tooltip.opacity === 0) {
							$tooltipEl.css('opacity', 0);
							return;
						}

						$tooltipEl.removeClass('left right center');
						if (tooltip.xAlign) {
							$tooltipEl.addClass(tooltip.xAlign);
						} else {
							$tooltipEl.addClass('no-transform');
						}

						var $borderList = $('<ul>').addClass('chart-tooltip-borders');
						tooltip.labelColors.forEach(function(labelColor) {
							$borderList.append(
								$('<li>').addClass('chart-tooltip-borders-item')
									.css('background', labelColor.backgroundColor)
							);
						});
						$tooltipEl.append($borderList);

						function getBody(bodyItem) {
							return bodyItem.lines;
						}

						// Set Text
						if (tooltip.body) {
							var titleLines = tooltip.title || [];
							var bodyLines = tooltip.body.map(getBody);

							var $titles = $('<div>').addClass('chart-tooltip-titles');
							bodyLines.forEach(function(title) {
								$titles.append($('<strong>').addClass('chart-tooltip-title').html(title));
							});
							$tooltipEl.append($titles);

							var $lines = $('<div>').addClass('chart-tooltip-lines');
							titleLines.forEach(function(line) {
								$lines.append($('<span>').addClass('chart-tooltip-line').html(line));
							});
							$tooltipEl.append($lines);
						}

						var positionY = this._chart.canvas.offsetTop;
						var positionX = this._chart.canvas.offsetLeft;

						// Display, position, and set styles for font
						$tooltipEl.css({
							'opacity': 1,
							'left': positionX + tooltip.caretX + 'px',
							'top': positionY + tooltip.caretY + 'px'
						});
					}
				}
			},
			plugins: [{
				afterUpdate: function(chart) {
					chart.ctx.shadowBlur = 20;
					chart.ctx.shadowColor = 'rgba(0, 0, 0, .04)';
					chart.ctx.shadowOffsetY = 10;
				}
			}]
		});
		this.initializeButtonsDonut(c);
	},
	chartDonut2: function() {
		if (this.length === 0) {
			return false
		}

		if (this.isMobile()) {
			this.attr('height', parseInt($(this).attr('width')) * .68);
		}

		var c = new Chart(this, {
			type: 'doughnut',
			data: {
				datasets: [{
					data: $(this).data().dataset,
					backgroundColor: ['#29CC97', '#FFCB68', '#FD668A'],
					borderWidth: 0
				}],
				labels: ['Dobrze', 'Średnio', 'Słabo']
			},
			options: {
				animation: {
					duration: 200
				},
				legend: false,
				cutoutPercentage: 75,
				scales: {
					xAxes: [{
						display: false
					}],
					yAxes: [{
						display: false
					}]
				},
				tooltips: {
					enabled: false,
					custom: function(tooltip) {
						// Tooltip Element
						var $tooltipEl = $('.chart-tooltip');

						if (!$tooltipEl.length) {
							$tooltipEl = $('<div>');
							$tooltipEl.addClass('chart-tooltip');
						} else {
							$tooltipEl.empty();
						}
						this._chart.canvas.parentNode.appendChild($tooltipEl[0]);

						// Hide if no tooltip
						if (tooltip.opacity === 0) {
							$tooltipEl.css('opacity', 0);
							return;
						}

						$tooltipEl.removeClass('left right center');
						if (tooltip.xAlign) {
							$tooltipEl.addClass(tooltip.xAlign);
						} else {
							$tooltipEl.addClass('no-transform');
						}

						var $borderList = $('<ul>').addClass('chart-tooltip-borders');
						tooltip.labelColors.forEach(function(labelColor) {
							$borderList.append(
								$('<li>').addClass('chart-tooltip-borders-item')
									.css('background', labelColor.backgroundColor)
							);
						});
						$tooltipEl.append($borderList);

						function getBody(bodyItem) {
							return bodyItem.lines;
						}

						// Set Text
						if (tooltip.body) {
							var titleLines = tooltip.title || [];
							var bodyLines = tooltip.body.map(getBody);

							var $titles = $('<div>').addClass('chart-tooltip-titles');
							bodyLines.forEach(function(title) {
								$titles.append($('<strong>').addClass('chart-tooltip-title').html(title));
							});
							$tooltipEl.append($titles);

							var $lines = $('<div>').addClass('chart-tooltip-lines');
							titleLines.forEach(function(line) {
								$lines.append($('<span>').addClass('chart-tooltip-line').html(line));
							});
							$tooltipEl.append($lines);
						}

						var positionY = this._chart.canvas.offsetTop;
						var positionX = this._chart.canvas.offsetLeft;

						// Display, position, and set styles for font
						$tooltipEl.css({
							'opacity': 1,
							'left': positionX + tooltip.caretX + 'px',
							'top': positionY + tooltip.caretY + 'px'
						});
					}
				}
			}
		});
		this.initializeButtonsDonut2(c);
	},
	chartLine: function() {
		if (this.length === 0) {
			return false
		}

		if (this.isMobile()) {
			this.attr('height', parseInt($(this).attr('width')) * 1.25);
		}

		var ctx = this[0].getContext('2d');
		var offset = this.isMobile() ? ctx.canvas.width * .025 : ctx.canvas.width * .09;

		var datasetsObj = $(this).data();
		var datasets = {
			green: datasetsObj.datasetGood,
			yellow: datasetsObj.datasetMedium,
			red: datasetsObj.datasetBad
		};

		var maxGlobal = Math.max.apply(null, datasets.green.concat(datasets.yellow, datasets.red));
		var height = ctx.canvas.height - 25;

		var gradients = {
			green: ctx.createLinearGradient(0,height * (1 - Math.max.apply(null, datasets.green) / maxGlobal),0,height),
			yellow: ctx.createLinearGradient(0,height * (1 - Math.max.apply(null, datasets.yellow) / maxGlobal),0,height),
			red: ctx.createLinearGradient(0,height * (1 - Math.max.apply(null, datasets.red) / maxGlobal),0,height)
		};

		gradients.green.addColorStop(0, 'rgba(172,249,133,.2)');
		gradients.green.addColorStop(.75, 'rgba(172,249,133,0)');
		gradients.yellow.addColorStop(0, 'rgba(255,198,109,.2)');
		gradients.yellow.addColorStop(.5, 'rgba(255,198,109,0)');
		gradients.red.addColorStop(0, 'rgba(253,101,145,.2)');
		gradients.red.addColorStop(.5, 'rgba(253,101,145,0)');

		var data = {
			type: 'line',
			data: {
				labels: ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN', ''],
				datasets: [{
					label: 'Dobrze',
					backgroundColor: gradients.green,
					borderColor: '#29CC97',
					data: datasets.green,
					pointHoverRadius: 4,
					borderCapStyle: 'round',
					pointBackgroundColor: 'transparent',
					pointBorderColor: 'transparent',
					pointHoverBackgroundColor: '#fff',
					pointHoverBorderColor: '#29CC97',
					pointBorderWidth: 3,
					pointHitRadius: this.isMobile() ? 25 : 50
				},{
					label: 'Średnio',
					backgroundColor: gradients.yellow,
					borderColor: '#FFCB68',
					data: datasets.yellow,
					pointRadius: 0,
					borderCapStyle: 'round',
					pointBorderColor: 'transparent',
					pointHoverBackgroundColor: '#fff',
					pointHoverBorderColor: '#FFCB68',
					pointBorderWidth: 3,
					pointHitRadius: this.isMobile() ? 25 : 50
				},{
					label: 'Słabo',
					backgroundColor: gradients.red,
					borderColor: '#FD668A',
					data: datasets.red,
					pointRadius: 0,
					borderCapStyle: 'round',
					pointBorderColor: 'transparent',
					pointHoverBackgroundColor: '#fff',
					pointHoverBorderColor: '#FD668A',
					pointBorderWidth: 3,
					pointHitRadius: this.isMobile() ? 25 : 50
				}]
			},
			options: {
				animation: {
					duration: 200
				},
				responsive: true,
				legend: false,
				tooltips: {
					enabled: false,
					mode: 'point',
					position: 'nearest',
					custom: function(tooltip) {
						// Tooltip Element
						var $tooltipEl = $('.chart-tooltip');

						if (!$tooltipEl.length) {
							$tooltipEl = $('<div>');
							$tooltipEl.addClass('chart-tooltip');
						} else {
							$tooltipEl.empty();
						}
						this._chart.canvas.parentNode.appendChild($tooltipEl[0]);

						// Hide if no tooltip
						if (tooltip.opacity === 0) {
							$tooltipEl.css('opacity', 0);
							return;
						}

						$tooltipEl.removeClass('left right center');

						var $borderList = $('<ul>').addClass('chart-tooltip-borders');
						tooltip.labelColors.forEach(function(labelColor) {
							$borderList.append(
								$('<li>').addClass('chart-tooltip-borders-item')
									.css('background', labelColor.borderColor)
							);
						});
						$tooltipEl.append($borderList);

						function getBody(bodyItem) {
							return bodyItem.lines;
						}

						// Set Text
						if (tooltip.body) {
							var titleLines = tooltip.title || [];
							var bodyLines = tooltip.body.map(getBody);

							var $titles = $('<div>').addClass('chart-tooltip-titles');
							bodyLines.forEach(function(title) {
								$titles.append($('<strong>').addClass('chart-tooltip-title').html(title));
							});
							$tooltipEl.append($titles);

							var $lines = $('<div>').addClass('chart-tooltip-lines');
							titleLines.forEach(function(line) {
								$lines.append($('<span>').addClass('chart-tooltip-line').html(line));
							});
							$tooltipEl.append($lines);
						}

						var positionY = this._chart.canvas.offsetTop;
						var positionX = this._chart.canvas.offsetLeft;

						// Display, position, and set styles for font
						$tooltipEl.css({
							'opacity': 1,
							'left': positionX + tooltip.caretX + 'px',
							'top': positionY + tooltip.caretY + 'px'
						});
					}
				},
				hover: {
					mode: 'point',
					intersect: false
				},
				scales: {
					xAxes: [{
						gridLines: {
							display: false,
							drawBorder: false
						},
						ticks: {
							labelOffset: offset,
							padding: 15,
							fontColor: '#A2A4AF',
							fontFamily: 'soin_sans_neue',
							fontSize: 11,
							fontStyle: 'bold'
						}
					}],
					yAxes: [{
						gridLines: {
							borderDash: [5],
							color: '#E5E6EB',
							drawBorder: false,
							tickMarkLength: 0,
							zeroLineColor: '#E5E6EB',
							zeroLineBorderDash: [5]
						},
						ticks: {
							padding: this.isMobile() ? 0 : -27,
							mirror: true,
							fontColor: '#A2A4AF',
							fontFamily: 'soin_sans_neue',
							fontSize: 11,
							fontStyle: 'bold'
						}
					}]
				}
			},
			plugins: [{
				afterUpdate: function(chart) {
					// We get the dataset and set the offset here

					$.each(chart.config.data.datasets, function (index, dataset) {
						// For every data in the dataset ...
						for (var i = 0; i < dataset._meta[Object.keys(dataset._meta)[0]].data.length; i++) {
							// We get the model linked to this data
							var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;

							// And add the offset to the `x` property
							model.x += offset;

							// .. and also to these two properties
							// to make the bezier curve fits the new graph
							model.controlPointNextX += offset;
							model.controlPointPreviousX += offset;
						}
					});
				}
			}]
		};

		var c = new Chart(this, data);
		this.initializeButtons(c);
	}
});
