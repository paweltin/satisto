$.fn.extend({
	floatLabel: function() {
		this.$formControl = this.find('.form-control');

		var self = this;
		this.$formControl
			.on('blur', function (e) {
				self.checkValue($(e.currentTarget), e.type);
			})
			.on('keyup', function (e) {
				self.checkValue($(e.currentTarget));
			});

		this.checkValue = function($el, eventType) {
			if ($el.val() !== '') {
				$el.addClass('form-control--filled');
			} else if (eventType === 'blur') {
				$el.removeClass('form-control--filled');
			}
		};

		this.$formControl.each((function(i, el) {
			this.checkValue($(el));
		}).bind(this));
	},
	formCheckButton: function() {
		this.find('.form-check-input').on('change', function(e) {
			$(e.currentTarget).closest('form').find('.btn[disabled]').removeAttr('disabled');
		});
	},
	formCheckRadio: function() {
		var $inputRemindToggle = $('.input-remind-toggle').hide();

		this.on('change', (function() {
			if (this.filter(':checked').val() !== '0') {
				$inputRemindToggle.fadeIn(200);
			} else {
				$inputRemindToggle.fadeOut(200);
			}
		}).bind(this));

		this.trigger('change');
	}
});
