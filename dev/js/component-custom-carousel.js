(function() {
	$('[data-custom-carousel]').each(function(i, el) {
		var data = $(el).data();

		$(el).slick({
			centerMode: true,
			centerPadding: 0,
			slidesToShow: data.slidesToShow,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1
					}
				}
			]
		}).on('beforeChange', function (e, slider, current, next) {
			$current = $(slider.$slides[next]);
			$current.prev().prev().removeClass('first prev next last').addClass('first');
			$current.prev().removeClass('first prev next last').addClass('prev');
			$current.removeClass('first prev next last');
			$current.next().removeClass('first prev next last').addClass('next');
			$current.next().next().removeClass('first prev next last').addClass('last');
		}).slick('slickGoTo', 0);
	});
})();
