module.exports = function (grunt) {
	var imageminJpegoptim = require('imagemin-jpegoptim');
	var imageminPngquant = require('imagemin-pngquant');

	grunt.initConfig({
		browserSync: {
			dev: {
				bsFiles: {
					src: [
						'dist/css/*.css',
						'dist/js/*.js',
						'dist/*.html'
					]
				},
				options: {
					server: true,
					port: 3000,
					open: false,
					reloadOnRestart: true,
					ui: {
						port: 2999
					},
					watchTask: true
				}
			}
		},
		browserify: {
			dist: {
				files: {
					'dist/js/index.js': [
						'dev/js/app.js'
					]
				}
			}
		},
		copy: {
			main: {
				files: [
					{
						expand: true,
						flatten: true,
						src: ['dev/fonts/*'],
						dest: 'dist/fonts',
						filter: 'isFile'
					},
					{
						expand: true,
						flatten: true,
						src: ['node_modules/slick-carousel/slick/ajax-loader.gif'],
						dest: 'dist/css',
						filter: 'isFile'
					},
					{
						expand: true,
						flatten: true,
						src: ['dev/favicon/*'],
						dest: 'dist',
						filter: 'isFile'
					},
					{
						expand: true,
						flatten: true,
						src: ['dev/img/splash-bg.png'],
						dest: 'dist/img',
						filter: 'isFile'
					}
				]
			}
		},
		htmlbuild: {
			dist: {
				src: 'dev/html/*.html',
				dest: 'dist/',
				options: {
					beautify: true,
					relative: true,
					basePath: false,
					scripts: {
						bundle: [
							'dist/js/index.js'
						]
					},
					styles: {
						bundle: [
							'dist/css/style.css'
						]
					},
					sections: {
						components: {
							account: {
								base: 'dev/html/components/account/base.html',
								filled: 'dev/html/components/account/filled.html',
								filledError: 'dev/html/components/account/filled-error.html'
							},
							box: {
								chartDonut2: 'dev/html/components/box/chart-donut-2.html',
								chartLine: 'dev/html/components/box/chart-line.html',
								chartLine2: 'dev/html/components/box/chart-line-2.html',
								dashboardMenu: 'dev/html/components/box/dashboard-menu.html',
								dropdown: 'dev/html/components/box/dropdown.html'
							},
							contact: {
								base: 'dev/html/components/contact/base.html',
								errors: 'dev/html/components/contact/errors.html'
							},
							card: {
								1: 'dev/html/components/card/card-1.html',
								2: 'dev/html/components/card/card-2.html',
								3: 'dev/html/components/card/card-3.html'
							},
							dashboard: {
								plan: 'dev/html/components/dashboard/plan.html',
								summary: 'dev/html/components/dashboard/summary.html',
								tabs: 'dev/html/components/dashboard/tabs.html',
								tabsReadonly: 'dev/html/components/dashboard/tabs-readonly.html'
							},
							hero: {
								top: 'dev/html/components/hero/top.html',
								bottom: 'dev/html/components/hero/bottom.html'
							},
							mail: {
								header: 'dev/html/components/mail/header.html',
								footer: 'dev/html/components/mail/footer.html',
								style: 'dev/html/components/mail/style.html'
							},
							modal: {
								accountRemove: 'dev/html/components/modal/account-remove.html',
								accountRemoveWhy: 'dev/html/components/modal/account-remove-why.html',
								pollCreate: 'dev/html/components/modal/poll-create.html',
								pollPreview: 'dev/html/components/modal/poll-preview.html'
							},
							message: {
								success: 'dev/html/components/message/success.html',
								danger: 'dev/html/components/message/danger.html',
								danger2: 'dev/html/components/message/danger-2.html'
							},
							poll: {
								step1: 'dev/html/components/poll/step-1.html',
								step2: 'dev/html/components/poll/step-2.html',
								step2Advanced: 'dev/html/components/poll/step-2-advanced.html',
								step3: 'dev/html/components/poll/step-3.html',
								step3Code: 'dev/html/components/poll/step-3-code.html'
							},
							report: {
								boxes: 'dev/html/components/report/boxes.html',
								top: 'dev/html/components/report/top.html'
							},
							splash: {
								pass: 'dev/html/components/splash/pass.html',
								passEmail: 'dev/html/components/splash/pass-email.html',
								passError: 'dev/html/components/splash/pass-error.html',
								passNew: 'dev/html/components/splash/pass-new.html',
								register: 'dev/html/components/splash/register.html',
								registerEmail: 'dev/html/components/splash/register-email.html',
								registerError: 'dev/html/components/splash/register-error.html',
								text: 'dev/html/components/splash/text.html',
								textError: 'dev/html/components/splash/text-error.html',
								textError2: 'dev/html/components/splash/text-error-2.html',
								image: 'dev/html/components/splash/image.html'
							},
							subheader: {
								contact: 'dev/html/components/subheader/contact.html',
								faq: 'dev/html/components/subheader/faq.html',
								step1: 'dev/html/components/subheader/step-1.html',
								step2: 'dev/html/components/subheader/step-2.html',
								step3: 'dev/html/components/subheader/step-3.html',
								title: 'dev/html/components/subheader/title.html',
								text: 'dev/html/components/subheader/text.html'
							},
							text: {
								content: 'dev/html/components/text/content.html',
								contentFaq: 'dev/html/components/text/content-faq.html',
								nav: 'dev/html/components/text/nav.html',
								navFaq: 'dev/html/components/text/nav-faq.html'
							}
						},
						layout: {
							header: 'dev/html/layout/header.html',
							header2: 'dev/html/layout/header-2.html',
							header3: 'dev/html/layout/header-3.html',
							header4: 'dev/html/layout/header-4.html',
							header5: 'dev/html/layout/header-5.html',
							header6: 'dev/html/layout/header-6.html',
							headerLoggedOut: 'dev/html/layout/header-logged-out.html',
							footer: 'dev/html/layout/footer.html'
						}
					}
				}
			}
		},
		imagemin: {
			dynamic: {
				options: {
					use: [
						imageminJpegoptim({max: 90}),
						imageminPngquant({quality: '65-80'})
					]
				},
				files: [
					{
						expand: true,
						cwd: 'dev/img',
						src: ['**/!(splash-bg).{png,jpg,gif,svg}'],
						dest: 'dist/img'
					}
				]
			}
		},
		postcss: {
			dev: {
				options: {
					map: {
						inline: true
					},
					processors: [
						require('autoprefixer')
					]
				},
				src: 'dist/css/style.css'
			},
			dist: {
				options: {
					map: true,
					processors: [
						require('autoprefixer'),
						require('cssnano')(),
						require('pixrem')(),
						require('postcss-color-rgba-fallback')(),
						require('postcss-opacity')(),
						require('postcss-pseudoelements')(),
						require('postcss-vmin')()
					]
				},
				src: 'dist/css/style.css'
			}
		},
		sass: {
			options: {
				sourceMap: true
			},
			dist: {
				files: {
					'dist/css/style.css': 'dev/scss/style.scss'
				}
			}
		},
		uglify: {
			dist: {
				mangle: false,
				beautiy: true,
				files: {
					'dist/js/index.js': [
						'node_modules/highlight.js/lib/highlight.js',
						'node_modules/highlightjs-line-numbers.js/dist/highlightjs-line-numbers.min.js',
						'dist/js/index.js'
					]
				}
			}
		},
		uncss: {
			dist: {
				options: {
					ignore: [
						/chart/,
						/checked/,
						/collaps/,
						/dropdown/,
						/form-check/,
						/form-control--filled/,
						/is-invalid/,
						/message-container/,
						/modal/,
						/platforms/,
						/hljs/
					]
				},
				files: {
					'dist/css/style.css': [
						'dist/styleguide.html',
						'dist/styleguide-splash.html',
						'dist/48-homepage.html'
					]
				}
			}
		},
		watch: {
			options: {
				atBegin: true,
				interrupt: false,
				livereload: true,
				spawn: false
			},
			browserify: {
				files: ['dev/js/*.js'],
				tasks: ['browserify', 'uglify']
			},
			sass: {
				files: ['dev/scss/*.scss'],
				tasks: ['sass', 'postcss:dev']
			},
			html: {
				files: ['dev/html/**/*.html'],
				tasks: ['htmlbuild']
			},
			images: {
				files: ['dev/img/**/*.{png,jpg,gif}'],
				tasks: ['imagemin']
			}
		}
	});

	require('load-grunt-tasks')(grunt);

	grunt.registerTask('default', ['copy', 'sass:dist', 'postcss:dist', 'browserify:dist', 'uglify', 'imagemin', 'htmlbuild', 'uncss']);
	grunt.registerTask('sync', ['browserSync', 'watch']);
};
